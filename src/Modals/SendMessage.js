/**
|--------------------------------------------------
|	E.A.O Designs
|	Saturday December 12, 2018
|	22: 48: 15 
|	SendMessage Component
|--------------------------------------------------
**/

import React, { Component } from 'react';
import { Button, Modal, Form, TextArea } from 'semantic-ui-react';

export default class SendMessage extends Component {
	state = { message: '' };
	render() {
		const { user } = this.props;
		const { message } = this.state;
		return (
			<Modal trigger={<Button positive content="Message" />} size="tiny">
				<Modal.Header>
					<span>Message </span>
					<span> {user[0].first_name}</span> <span> {user[0].last_name}</span>
				</Modal.Header>
				<Modal.Content>
					<Form>
						<Form.Field>
							<TextArea
								autoHeight
								placeholder="Type message here..."
								value={message}
								onChange={(e, d) => this.setState({ message: d.value })}
							/>
						</Form.Field>
						<Form.Button type="submit" content="Send" />
					</Form>
				</Modal.Content>
			</Modal>
		);
	}
}
