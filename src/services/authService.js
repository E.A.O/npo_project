/**
|--------------------------------------------------
|	E.A.O Designs
|	Monday December 12, 2018
|	14: 42: 48 
|	authService Component
|--------------------------------------------------
**/

import axios from 'axios';
import { Constants } from '../config/Constants';

export const isAuthenticated = () => {
	axios.get(Constants.app.BASE_API_URL);
};

export const verifyUser = async (username, password) => {
	try {
		// axios.post(); //post to jesse to furthur process
		return await axios.get(Constants.app.BASE_API_URL);
	} catch (error) {
		return error;
	}
};

export const registerUser = async (formFieldValues) => {
	try {
		// axios.post(); //post to jesse to furthur process
		return await axios.get(Constants.app.BASE_API_URL);
	} catch (error) {
		return error;
	}
};

export const formFieldValidator = (formField) => {
	let response = [
		{ firstNameError: false, firstNameErrorMsg: '' },
		{ lastNameError: false, lastNameErrorMsg: '' },
		{ emailError: false, emailErrorMsg: '' },
		{ telephoneNumberError: false, telephoneNumberErrorMsg: '' },
		{ passwordError: false, passwordErrorMsg: '' },
		{ confirmPasswordError: false, confirmPasswordErrorMsg: '' },
		{ secreteCodeError: false, secreteCodeErrorMsg: '' },
		{ errorFound: false }
	];
	if (formField.firstName.length < 1) {
		response[0].firstNameError = true;
		response[0].firstNameErrorMsg = 'First name Field cannot be empty';
		response[7].errorFound = true;
	} else if (!/^[0-9a-zA-Z ]+$/.test(formField.firstName)) {
		response[0].firstNameError = true;
		response[0].firstNameErrorMsg = 'First name contains illigal characters';
		response[7].errorFound = true;
	} else {
		response[0].firstNameError = false;
		response[0].firstNameErrorMsg = '';
	}

	if (formField.lastName.length < 1) {
		response[1].lastNameError = true;
		response[1].lastNameErrorMsg = 'Last name Field cannot be empty';
		response[7].errorFound = true;
	} else if (!/^[0-9a-zA-Z ]+$/.test(formField.lastName)) {
		response[1].lastNameError = true;
		response[1].lastNameErrorMsg = 'Last name contains illigal characters';
		response[7].errorFound = true;
	} else {
		response[1].lastNameError = false;
		response[1].lastNameErrorMsg = '';
	}

	if (formField.email.length < 5 || !/.com/.test(formField.email)) {
		response[2].emailError = true;
		response[2].emailErrorMsg = 'Invalid Email';
		response[7].errorFound = true;
	} else if (!/^[0-9a-zA-Z@$_. ]+$/.test(formField.email)) {
		response[2].emailError = true;
		response[2].emailErrorMsg = 'Email Field contains illigal characters';
		response[7].errorFound = true;
	} else if (!/@/.test(formField.email)) {
		response[2].emailError = true;
		response[2].emailErrorMsg = 'Invalid Email. Email should contain @';
		response[7].errorFound = true;
	} else {
		response[2].emailError = false;
		response[2].emailErrorMsg = '';
	}

	if (formField.telephoneNumber.length < 10) {
		response[3].telephoneNumberError = true;
		response[3].telephoneNumberErrorMsg = 'Invalid telephone number';
		response[7].errorFound = true;
	} else if (!/^[0-9+]+$/.test(formField.telephoneNumber)) {
		response[3].telephoneNumberError = true;
		response[3].telephoneNumberErrorMsg = 'Telephone number must be numbers only';
		response[7].errorFound = true;
	} else {
		response[3].telephoneNumberError = false;
		response[3].telephoneNumberErrorMsg = '';
	}

	if (formField.password.length < 1) {
		response[4].passwordError = true;
		response[4].passwordErrorMsg = 'Password Field cannot be empty';
		response[7].errorFound = true;
	} else if (!/^[0-9a-zA-Z@]+$/.test(formField.password)) {
		response[4].passwordError = true;
		response[4].passwordErrorMsg = 'Password Field contains illigal characters';
		response[7].errorFound = true;
	} else {
		response[4].passwordError = false;
		response[4].passwordErrorMsg = '';
	}

	if (formField.confirmPassword.length < 1) {
		response[5].confirmPasswordError = true;
		response[5].confirmPasswordErrorMsg = 'Password Field cannot be empty';
		response[7].errorFound = true;
	} else if (!/^[0-9a-zA-Z@]+$/.test(formField.confirmPassword)) {
		response[5].confirmPasswordError = true;
		response[5].confirmPasswordErrorMsg = 'Password Field contains illigal characters';
		response[7].errorFound = true;
	} else if (formField.password !== formField.confirmPassword) {
		response[5].confirmPasswordError = true;
		response[5].confirmPasswordErrorMsg = 'Password fields do not match';
		response[7].errorFound = true;
	} else {
		response[5].confirmPasswordError = false;
		response[5].confirmPasswordErrorMsg = '';
	}

	if (formField.secreteCode.length < 1) {
		response[6].secreteCodeError = true;
		response[6].secreteCodeErrorMsg = 'Please acquire secrete code from supervisor';
		response[7].errorFound = true;
	} else if (!/^[0-9a-zA-Z]+$/.test(formField.secreteCode)) {
		response[6].secreteCodeError = true;
		response[6].secreteCodeErrorMsg = 'Password Field contains illigal characters';
		response[7].errorFound = true;
	} else {
		response[6].secreteCodeError = false;
		response[6].secreteCodeErrorMsg = '';
	}

	return response;
};
