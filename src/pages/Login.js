/**
|--------------------------------------------------
|	E.A.O Designs
|	Monday December 12, 2018
|	14: 26: 53 
|	Login Component
|--------------------------------------------------
**/
import React, { Component } from 'react';
import { Form, Image, Header, Segment, Message } from 'semantic-ui-react';
import { verifyUser } from '../services/authService';
import { Link } from 'react-router-dom';

export default class Login extends Component {
	constructor(props) {
		super(props);
		this.state = { username: '', password: '', error: '', loading: false };
	}
	render() {
		return (
			<div
				style={{
					height: '100vh',
					width: '100vw',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center'
				}}
				className="login"
			>
				<div style={{ display: 'flex', flexDirection: 'column' }}>
					<Segment inverted>
						<Header as="h2">
							<Image circular src={require('../imgs/children.jpg')} style={{ width: '70px', height: '60px' }} /> Women
							Empowerment
						</Header>
					</Segment>

					<Segment stacked>
						<Form size="large" style={{ width: '400px' }} onSubmit={(e) => this.login(e)}>
							<Form.Input
								icon="user"
								iconPosition="left"
								placeholder="User Name"
								value={this.state.username}
								onChange={(e) => this.setLoginFormValue('username', e.target.value)}
							/>
							<Form.Input
								icon="lock"
								iconPosition="left"
								placeholder="Password"
								type="password"
								value={this.state.password}
								onChange={(e) => this.setLoginFormValue('password', e.target.value)}
							/>
							<Form.Button fluid={true} loading={this.state.loading} type="submit" color="grey">
								Login
							</Form.Button>
						</Form>
						<Message>
							Register to become a member{' '}
							<Link to="/register" style={{ paddingLeft: '5px' }}>
								Sign Up
							</Link>
						</Message>
						{this.state.error !== '' ? (
							<Message error onDismiss={() => this.setState({ error: '' })} header={this.state.error} />
						) : null}
					</Segment>
				</div>
			</div>
		);
	}

	async login(e) {
		e.preventDefault();
		this.setState({ loading: true });
		if (this.state.username === '') {
			this.setState({ error: 'Username field cannot be empty', loading: false });
		} else if (this.state.password === '') {
			this.setState({ error: 'Password field cannot be empty', loading: false });
		} else if (!/^[0-9a-zA-Z@]+$/.test(this.state.username)) {
			this.setState({ error: 'Username name field contains illigal character', loading: false });
		} else if (!/^[0-9a-zA-Z@]+$/.test(this.state.password)) {
			this.setState({ error: 'Password name field contains illigal character', loading: false });
		} else {
			const verify = await verifyUser(this.state.username, this.state.password);
			if (verify.status === 200) {
				this.setState({ loading: false, error: '' });
				console.log(verify);
				// alert('worked');
			} else {
				this.setState({
					error: 'Connection Problem. Be sure you are connected',
					loading: false
				});
			}
		}
	}

	setLoginFormValue(name, value) {
		if (this.state.error !== '') {
			this.setState({ error: '' });
		}
		this.setState({
			[name]: value
		});
	}
}
