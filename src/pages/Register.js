/**
|--------------------------------------------------
|	E.A.O Designs
|	Wednesday December 12, 2018
|	11: 03: 48 
|	Register Component
|--------------------------------------------------
**/

import React, { Component } from 'react';
import { Form, Segment, Header, Image, Message, List } from 'semantic-ui-react';
import { formFieldValidator, registerUser } from '../services/authService';
import { Link } from 'react-router-dom';

export default class Register extends Component {
	constructor(props) {
		super(props);
		this.state = {
			formFields: {
				firstName: '',
				lastName: '',
				email: '',
				telephoneNumber: '',
				password: '',
				confirmPassword: '',
				secreteCode: ''
			},
			errorFound: false,
			errorMessage: '',
			errors: {
				firstNameError: false,
				firstNameErrorMsg: '',
				lastNameError: false,
				lastNameErrorMsg: '',
				emailError: false,
				emailErrorMsg: '',
				telephoneNumberError: false,
				telephoneNumberErrorMsg: '',
				passwordError: false,
				passwordErrorMsg: '',
				confirmPasswordError: false,
				confirmPasswordErrorMsg: '',
				secreteCodeError: false,
				secreteCodeErrorMsg: ''
			},
			loading: false
		};
	}
	render() {
		return (
			<div
				style={{
					height: '100vh',
					width: '100vw',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center'
				}}
				className="register" //yet to style background
			>
				<div style={{ display: 'flex', flexDirection: 'column' }}>
					<Segment>
						<Header as="h2">
							<Image circular src={require('../imgs/children.jpg')} style={{ width: '90px', height: '60px' }} /> Women
							Empowerment
						</Header>
					</Segment>
					<Message floating>
						<Form onSubmit={(e) => this.register(e)}>
							<Form.Group widths="equal">
								<Form.Input
									icon="user"
									iconPosition="left"
									placeholder="First Name"
									value={this.state.formFields.firstName}
									error={this.state.errors.firstNameError}
									onChange={(e) => this.setLoginFormValue('firstName', e.target.value)}
								/>
								<Form.Input
									icon="user"
									iconPosition="left"
									placeholder="Last Name"
									value={this.state.lastName}
									error={this.state.errors.lastNameError}
									onChange={(e) => this.setLoginFormValue('lastName', e.target.value)}
								/>
							</Form.Group>

							<Form.Input
								icon="envelope"
								iconPosition="left"
								placeholder="Email"
								value={this.state.email}
								error={this.state.errors.emailError}
								onChange={(e) => this.setLoginFormValue('email', e.target.value)}
							/>
							<Form.Input
								icon="address book"
								iconPosition="left"
								placeholder="Telephone Number"
								value={this.state.telephoneNumber}
								error={this.state.errors.telephoneNumberError}
								onChange={(e) => this.setLoginFormValue('telephoneNumber', e.target.value)}
							/>
							<Form.Input
								icon="lock"
								iconPosition="left"
								placeholder="Password"
								type="password"
								value={this.state.password}
								error={this.state.errors.passwordError}
								onChange={(e) => this.setLoginFormValue('password', e.target.value)}
							/>
							<Form.Input
								icon="lock"
								iconPosition="left"
								placeholder="Confirm Password"
								type="password"
								value={this.state.confirmPassword}
								error={this.state.errors.confirmPasswordError}
								onChange={(e) => this.setLoginFormValue('confirmPassword', e.target.value)}
							/>
							<Form.Input
								icon="id badge"
								iconPosition="left"
								placeholder="Membership Number"
								value={this.state.secreteCode}
								error={this.state.errors.secreteCodeError}
								onChange={(e) => this.setLoginFormValue('secreteCode', e.target.value)}
							/>
							<Form.Button fluid={true} loading={this.state.loading} type="submit" color="grey">
								Register
							</Form.Button>
							<Message style={{ display: 'flex', justifyContent: 'center' }}>
								Already a member?{' '}
								<Link to="/login" style={{ paddingLeft: '5px' }} href="#">
									{' '}
									Login
								</Link>
							</Message>
							{this.state.errors.firstNameError ||
							this.state.errors.lastNameError ||
							this.state.errors.emailError ||
							this.state.errors.telephoneNumberError ||
							this.state.errors.passwordError ||
							this.state.errors.confirmPasswordError ||
							this.state.errorFound ||
							this.state.errors.secreteCodeError ? (
								<Message error style={{ display: 'flex', justifyContent: 'center' }}>
									<List as="ul">
										{this.state.errors.firstNameError === true ? (
											<List.Item as="li">{this.state.errors.firstNameErrorMsg}</List.Item>
										) : null}
										{this.state.errors.lastNameError === true ? (
											<List.Item as="li">{this.state.errors.lastNameErrorMsg}</List.Item>
										) : null}
										{this.state.errors.emailError === true ? (
											<List.Item as="li">{this.state.errors.emailErrorMsg}</List.Item>
										) : null}
										{this.state.errors.telephoneNumberError === true ? (
											<List.Item as="li">{this.state.errors.telephoneNumberErrorMsg}</List.Item>
										) : null}
										{this.state.errors.passwordErrorMsg !== '' ? (
											<List.Item as="li">{this.state.errors.passwordErrorMsg}</List.Item>
										) : null}
										{this.state.errors.confirmPasswordError === true ? (
											<List.Item as="li">{this.state.errors.confirmPasswordErrorMsg}</List.Item>
										) : null}
										{this.state.errors.secreteCodeError === true ? (
											<List.Item as="li">{this.state.errors.secreteCodeErrorMsg}</List.Item>
										) : null}
										{this.state.errorMessage !== '' ? <List.Item as="li">{this.state.errorMessage}</List.Item> : null}
									</List>
								</Message>
							) : null}
						</Form>
					</Message>
				</div>
			</div>
		);
	}

	setLoginFormValue = (field, value) => {
		this.setState({
			formFields: {
				...this.state.formFields,
				[field]: value
			},
			errors: {
				...this.state.errors,
				[field + 'Error']: false,
				[field + 'ErrorMsg']: ''
			},
			errorMessage: '',
			errorFound: false
		});
	};

	async register(event) {
		event.preventDefault();
		this.setState({ loading: true });
		const resp = await formFieldValidator(this.state.formFields);
		this.setState(
			{
				errors: {
					firstNameError: resp[0].firstNameError,
					firstNameErrorMsg: resp[0].firstNameErrorMsg,
					lastNameError: resp[1].lastNameError,
					lastNameErrorMsg: resp[1].lastNameErrorMsg,
					emailError: resp[2].emailError,
					emailErrorMsg: resp[2].emailErrorMsg,
					telephoneNumberError: resp[3].telephoneNumberError,
					telephoneNumberErrorMsg: resp[3].telephoneNumberErrorMsg,
					passwordError: resp[4].passwordError,
					passwordErrorMsg: resp[4].passwordErrorMsg,
					confirmPasswordError: resp[5].confirmPasswordError,
					confirmPasswordErrorMsg: resp[5].confirmPasswordErrorMsg,
					secreteCodeError: resp[6].secreteCodeError,
					secreteCodeErrorMsg: resp[6].secreteCodeErrorMsg
				},
				errorFound: resp[7].errorFound
			},
			async () => {
				if (this.state.errorFound === false) {
					let sendFormFieldValues = await registerUser(this.state.formFields);
					if (sendFormFieldValues.status === 200) {
						this.setState({ loading: false, errorMessage: '' });
						console.log(sendFormFieldValues);
					} else {
						this.setState({
							errorFound: true,
							errorMessage: 'Connection Problem. Be sure you are connected',
							loading: false
						});
					}
				} else {
					this.setState({ loading: false, errorFound: false });
				}
			}
		);
	}
}
