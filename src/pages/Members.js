/**
|--------------------------------------------------
|	E.A.O Designs
|	Saturday December 12, 2018
|	03: 30: 52 
|	Members Component
|--------------------------------------------------
**/
import React, { Component } from 'react';
import { Table, Pagination, Message, List, Button } from 'semantic-ui-react';
import SendMessage from '../Modals/SendMessage';
import members from '../services/members';

export default class Members extends Component {
	state = {
		members: [],
		data: [],
		user: [],
		details: false
	};

	componentDidMount() {
		this.setState({
			members: members,
			data: members.filter((members) => members.id > 0 && members.id <= 13)
		});
	}
	render() {
		const { data, user, details } = this.state;
		return (
			<div
				style={{
					height: '100vh',
					display: 'flex',
					flexDirection: 'row',
					width: '100vw'
				}}
			>
				<div
					style={{
						display: 'flex',
						flexDirection: 'column',
						justifyContent: 'space-between',
						overflow: 'auto',
						width: details ? null : '100%'
					}}
				>
					<Table selectable basic style={{ marginTop: '0px' }}>
						<Table.Header>
							<Table.Row>
								<Table.HeaderCell>#</Table.HeaderCell>
								<Table.HeaderCell>Members</Table.HeaderCell>
								<Table.HeaderCell>E-mail</Table.HeaderCell>
								<Table.HeaderCell>Gender</Table.HeaderCell>
								<Table.HeaderCell>Telephone No.</Table.HeaderCell>
								<Table.HeaderCell>Date Joined</Table.HeaderCell>
							</Table.Row>
						</Table.Header>

						<Table.Body>
							{data ? (
								data.map((data) => (
									<Table.Row key={data.id} onClick={() => this.userData(data)}>
										<Table.Cell>{data.id}</Table.Cell>
										<Table.Cell>
											<span>{data.first_name}</span>
											<span> {data.last_name}</span>
										</Table.Cell>
										<Table.Cell>{data.email}</Table.Cell>
										<Table.Cell>{data.gender}</Table.Cell>
										<Table.Cell>{data.telephone}</Table.Cell>
										<Table.Cell>{data.date}</Table.Cell>
									</Table.Row>
								))
							) : null}
						</Table.Body>
					</Table>
					<Pagination
						style={{ marginBottom: '2px' }}
						defaultActivePage={1}
						onPageChange={(e, d) => this.handlePage(d.activePage)}
						totalPages={members.length / 13}
					/>
				</div>
				{details === true ? (
					<div style={{ marginLeft: '20px', overflow: 'auto' }}>
						<Message
							onDismiss={() => this.setState({ details: false })}
							style={{ width: '30vw', display: 'flex', flexDirection: 'column' }}
						>
							<Message.Header style={{ display: 'flex', justifyContent: 'center' }}>User Personal Info</Message.Header>
							{user ? (
								user.map((user) => (
									<List key={user.id} size="big">
										<List.Item>
											<span>Put image here </span>
										</List.Item>
										<List.Item>
											<span>Name: </span>
											<span> {user.first_name}</span>
											<span> {user.last_name}</span>
										</List.Item>
										<List.Item>
											<span>E-Mail: </span>
											<span> {user.email}</span>
										</List.Item>
										<List.Item>
											<span>Gender: </span>
											<span> {user.gender}</span>
										</List.Item>
										<List.Item>
											<span>Telephone Number: </span>
											<span> {user.telephone}</span>
										</List.Item>
										<List.Item>
											<span>Membership Date: </span>
											<span> {user.date}</span>
										</List.Item>
									</List>
								))
							) : null}
							<Button.Group>
								<SendMessage user={user} />
								<Button.Or />
								<Button negative content="Delete " />
							</Button.Group>
						</Message>
					</div>
				) : null}
			</div>
		);
	}

	handlePage = (activePage) => {
		this.setState({
			data: this.state.members.filter((members) => members.id > activePage * 13 - 13 && members.id <= activePage * 13)
		});
	};

	userData = (data) => {
		this.setState({
			details: true,
			user: [ data ]
		});
	};
}
