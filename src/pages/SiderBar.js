/**
|--------------------------------------------------
|	E.A.O Designs
|	Saturday December 12, 2018
|	03: 17: 19 
|	SiderBar Component
|--------------------------------------------------
**/
import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';

export default class renderSidebar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			toggleSideBar: false,
			activeLink: '',
			collapse: 'Collapse',
			members: 'Members',
			post: 'Create Post',
			message: 'Message'
		};
	}

	render() {
		const { members, post, message, collapse, toggleSideBar, activeLink } = this.state;
		return (
			<div style={{ display: 'flex', flexDirection: 'column', position: 'static' }}>
				<div className="sidebar">
					<div style={{ width: toggleSideBar ? '150px' : '60px' }}>
						<div
							className="menu-item-list"
							style={{
								backgroundColor: activeLink === '/dashboard/members' ? '#5b3c88' : null,
								color: activeLink === '/dashboard/members' ? '#fff' : null
							}}
							onClick={() => this.handleRoute('/dashboard/members')}
						>
							<Icon name="user outline" size="big" />
							{toggleSideBar ? <div className="menu-text">{members}</div> : null}
						</div>
						<div
							className="menu-item-list"
							style={{
								backgroundColor: activeLink === '/dashboard/post' ? '#5b3c88' : null,
								color: activeLink === '/dashboard/post' ? '#fff' : null
							}}
							onClick={() => this.handleRoute('/dashboard/post')}
						>
							<Icon name="edit outline" size="big" />
							{toggleSideBar ? <div className="menu-text">{post}</div> : null}
						</div>
						<div
							className="menu-item-list"
							style={{
								backgroundColor: activeLink === '/dashboard/message' ? '#5b3c88' : null,
								color: activeLink === '/dashboard/message' ? '#fff' : null
							}}
							onClick={() => this.handleRoute('/dashboard/message')}
						>
							<Icon name="envelope outline" size="big" />
							{toggleSideBar ? <div className="menu-text">{message}</div> : null}
						</div>
					</div>
					<div className="menu-item-list" onClick={this.handleSideBar}>
						<Icon name={toggleSideBar ? 'angle double left' : 'angle double right'} size="big" />
						{toggleSideBar ? <div className="menu-text">{collapse}</div> : null}
					</div>
				</div>
			</div>
		);
	}

	handleRoute = (route) => {
		this.props.history.push(route);
		this.setState({
			activeLink: route
		});
	};

	handleSideBar = () => {
		this.setState({
			toggleSideBar: !this.state.toggleSideBar
		});
	};
}
