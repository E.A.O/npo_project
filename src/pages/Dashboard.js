/**
|--------------------------------------------------
|	E.A.O Designs
|	Monday December 12, 2018
|	14: 27: 02 
|	Dashboard Component
|--------------------------------------------------
**/
import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './Login';
import ErrorPage from './ErrorPage';
import Register from './Register';
import Sidebar from './SiderBar';
import Members from './Members';
import Announcement from './Announcement';
import Post from './Post';

export default class Dashboard extends Component {
	render() {
		return (
			<div style={{ display: 'flex', flexDirection: 'row' }}>
				<Sidebar {...this.props} />
				<Switch>
					<Route path="/dashboard/login" component={Login} />
					<Route path="/dashboard/register" component={Register} />
					<Route path="/dashboard/members" component={Members} />
					<Route path="/dashboard/message" component={Announcement} />
					<Route path="/dashboard/post" component={Post} />
					<Route component={ErrorPage} />
				</Switch>
			</div>
		);
	}
}
