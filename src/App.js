/**
|--------------------------------------------------
|	E.A.O Designs
|	Monday December 12, 2018
|	14: 26: 39 
|	App Component
|--------------------------------------------------
**/
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import Register from './pages/Register';
import ErrorPage from './pages/ErrorPage';

const App = () => {
	return (
		<BrowserRouter>
			<Switch>
				<Route path="/login" component={Login} />
				<Route path="/register" component={Register} />
				<Route path="/dashboard" component={Dashboard} />
				<Route component={ErrorPage} />
			</Switch>
		</BrowserRouter>
	);
};

export default App;
