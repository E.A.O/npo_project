/**
|--------------------------------------------------
|	E.A.O Designs
|	Monday December 12, 2018
|	14: 44: 31 
|	constants Component
|--------------------------------------------------
**/

export const Constants = {
	app: {
		BASE_API_URL: 'https://my-json-server.typicode.com/eaodesigns/api/db'
	},

	apiRoutes: {
		login: '/login',
		dashboard: '/dashboard'
	}
};
